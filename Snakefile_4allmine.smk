
# feature: fastq from csv file, modules, SLURM, report
# feature: fastq from csv file, modules, SLURM, report

import pandas as pd
from snakemake.utils import min_version
# snakemake built-in report function requires min version 5.1
min_version("5.1.0")

#report: "report/workflow.rst"

#read the sample file using pandas lib (sample names+ fastq names) and crezate index using the sample name
samples = pd.read_table(config["samplesfile"], sep='\t', dtype=str, comment='#').set_index(["SampleName"], drop=False)

#common utilities
#---------------- functions to get fastq files ------------------------------------------
#function return fastq1 and fastq2 files and adds the fastq path
# works with SE, PE or mix of PE+SE fastq
def get_fastq(wildcards):
    return config["fq_dir"]+"/"+samples.loc[(wildcards.sample), ["fq1", "fq2"]].dropna()

#function return only the fastq1 file and adds the fastq path
def get_fastq1(wildcards):
    return config["fq_dir"]+"/"+samples.loc[(wildcards.sample), ["fq1"]].dropna()

#function return only the fastq2 file and adds the fastq path
def get_fastq2(wildcards):
    return config["fq_dir"]+"/"+samples.loc[(wildcards.sample), ["fq2"]].dropna()

#get sample name
#def get_sample(wildcards):
#    return samples.loc[(wildcards.sample), ["SampleName"]].dropna()
#---------------------------------------------------------------------------------------

#------------------ annovar db generation ------------------
#rm -fr avdb
#mkdir avdb
#singularity exec /nosave/project/gafl/tools/containers/AllMine_2.simg gff3ToGenePred vps4add2.gff avdb/AV_refGene.txt
#singularity exec /nosave/project/gafl/tools/containers/AllMine_2.simg perl /opt/annovar/retrieve_seq_from_fasta.pl --format refGene --seqfile /work/project/gafl/ReDD/ResistanceVirus/illumina/ref_vps4/CM3.6.1_pseudomol_chr11_vps4gene.fasta avdb/AV_refGene.txt --out avdb/AV_refGeneMrna.fa
#
cwd = os.getcwd() + "/"

# modules loading...
# include : cwd + "modules/module_dna_pe_up2mapping.smk"
# for RNA
include : cwd + "modules/module_rna_pe_up2mapping.smk"
include : cwd + "modules/module_allmine.smk"

rule final_output:
    input:
        #expand("{outdir}/fastp/{sample}_1_trim.fastq.gz", outdir=config["outdir"], sample=samples['SampleName']),
        #expand("{outdir}/fastp/{sample}_2_trim.fastq.gz", outdir=config["outdir"], sample=samples['SampleName']),
        #expand("{outdir}/fastqc/{sample}.OK.done", outdir=config["outdir"], sample=samples['SampleName']),
        #"{outdir}/variant/SNPs4all_freebayes.vcf".format(outdir=config["outdir"]),
        #expand("{outdir}/variant/{sample}_freebayes.vcf", outdir=config["outdir"], sample=samples['SampleName']),
        #"{outdir}/bams.list".format(outdir=config["outdir"]),
        #expand("{outdir}/mapped/{sample}_sorted.bam", outdir=config["outdir"], sample=samples['SampleName']),
        #expand("{outdir}/mapped/{sample}_sorted_parsed.bam.bai", outdir=config["outdir"], sample=samples['SampleName']),
        "{outdir}/multiqc/multiqc_report_fastqc.html".format(outdir=config["outdir"]),
        "{outdir}/multiqc/multiqc_report_bam.html".format(outdir=config["outdir"]),
        expand("{outdir}/variant/{sample}_varscan_phased.vcf",outdir=config["outdir"], sample=samples['SampleName']),
        "{outdir}/Non_synonymous_variants_summary.tab".format(outdir=config["outdir"]),
        "{outdir}/Coverage_Track.tab".format(outdir=config["outdir"]),
        "{outdir}/Run_report.html".format(outdir=config["outdir"])





