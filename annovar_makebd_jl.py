#!/usr/bin/env python3
#
# This script  build the Annovar database
# for your reference genome and annotation
# config.yaml use as input

#import yaml
import sys
import os

#cat Braker2_mRNA_Prediction-chr1-1..43620705.gff3 >jac_cat.gff3
#for i in chr2 chr3 chr4 chr5 chr6 chr7 chr8 Super-Scaffold_90 Super-Scaffold_99
#do
#grep -v '^##gff-version 3' Braker2_mRNA_Prediction-${i}*.gff3 >>jac_cat.gff3
#done


#mygenome="chr4.fa"
#myannot="Braker2_mRNA_Prediction-chr4-1..19069523.gff3"
myannot="/work2/project/gafl/Data/Euphorbiaceae/Manihot_esculenta/DNA/Ref_Genome/GCF_001659605.1_Manihot_esculenta_v6_genomic.gff"
#myannot="jac_cat.gff3"
mygenome="/work2/project/gafl/Data/Euphorbiaceae/Manihot_esculenta/DNA/Ref_Genome/GCF_001659605.1_Manihot_esculenta_v6_genomic.fna"

allmine="singularity exec -B /work2/project/gafl /work2/project/gafl/tools/containers/AllMine_2.sif"

# gff3 must start with the line: ##gff-version 3
print("\33[93mAnnovar data base building may take a while.\33[0m\n")

#config = yaml.safe_load(open('config.yaml'))
os.system('rm -rf avdb/')
os.system('mkdir avdb')

if myannot.endswith(".gff"):
    os.system(allmine + " gff3ToGenePred " + myannot + " avdb/AV_refGene.txt")
elif myannot.endswith(".gtf"):
    os.system(allmine + " gtfToGenePred " + myannot + " avdb/AV_refGene.txt")

os.system(allmine + \
" perl /opt/annovar/retrieve_seq_from_fasta.pl --format refGene --seqfile " + \
mygenome + " avdb/AV_refGene.txt --out avdb/AV_refGeneMrna.fa")

print('\n\n\33[32mDone! Database builded successfully.\33[0m\n')


