##### load rules #####

rule fastp_pe:
    input:
        R1  = get_fastq1,
        R2  = get_fastq2
        #R1 = lambda wildcards: "../data/"+samples.fq1[wildcards.sample],
        #R2 = lambda wildcards: "../data/"+samples.fq2[wildcards.sample]
        #R1  = lambda wildcards: config["fq_dir"]+"/"+samples.loc[(wildcards.sample), ["fq1"]].dropna(),
        #R2  = lambda wildcards: config["fq_dir"]+"/"+samples.loc[(wildcards.sample), ["fq2"]].dropna()
    output:
        R1 = "{outdir}/fastp/{{sample}}_1_trim.fastq.gz".format(outdir=config["outdir"]),
        R2 = "{outdir}/fastp/{{sample}}_2_trim.fastq.gz".format(outdir=config["outdir"])
    message: "Running fastp on files {input.R1} and {input.R2} \n"
    params:
        fqdir      = config["fq_dir"],
        outdir     = config["outdir"],
        modules    = config["MODULES"],
        fastp_bin  = config["fastp_bin"],
        bind       = config["BIND"],
        json       = config["outdir"]+"/fastp/{sample}_trim.json",
        html       = config["outdir"]+"/fastp/{sample}_trim.html"
    shell:
       """
        singularity exec {params.bind} {params.fastp_bin} fastp \
        -i {input.R1} \
        -I {input.R2} \
        -o {output.R1} \
        -O {output.R2} \
        -h {params.html} \
        -j {params.json} \
        --max_len1 350 \
        --correction \
        --cut_mean_quality 20 \
        --cut_window_size 4 \
        --low_complexity_filter \
        --complexity_threshold 30 \
        -w 4
        rm -f {params.html} {params.json}
        exit 0
         
        singularity exec {params.bind} {params.fastp_bin} fastp \
        -i {input.R1} \
        -I {input.R2} \
        -o {output.R1} \
        -O {output.R2} \
        --json={params.json} \
        --html={params.html} \
        --trim_tail1=1 \
        --cut_front \
        --cut_tail \
        --length_required 35 \
        --average_qual 20 \
        --length_limit 400 \
        --correction \
        --cut_mean_quality 10 \
        --low_complexity_filter \
        --complexity_threshold 20 \
        --thread 4
        #rm -f {params.html} {params.json}
        """

rule bams_list :
    input:
        lambda wildcards: expand(expand("{outdir}/mapped/{sample}_sorted.bam", outdir=config["outdir"], sample=samples['SampleName']))
    output:
        mylist = "{outdir}/bams.list".format(outdir=config["outdir"])
    params:
        bamsp       = "{outdir}/mapped".format(outdir=config["outdir"]),
        outlist     = config["outdir"]
    shell:"""
        #ls {params.bamsp}/*.bam > {params.outlist}/bams.list
        echo {input}|tr -s '[:space:]' '\n' >{params.outlist}/bams.list
        """

rule bwa_index:
    input:
        genome = config["REFPATH"] + "/" + config["GENOME"]
    output:
        config["REFPATH"] + "/" + config["GENOME"] + ".amb",
        config["REFPATH"] + "/" + config["GENOME"] + ".ann",
        config["REFPATH"] + "/" + config["GENOME"] + ".bwt",
        config["REFPATH"] + "/" + config["GENOME"] + ".pac",
        config["REFPATH"] + "/" + config["GENOME"] + ".sa",
        config["REFPATH"] + "/" + config["GENOME"] + ".fai"
    params:
        bind         = config["BIND"],
        bwa_bin      = config["bwa_bin"],
        samtools_bin = config["samtools_bin"]
    message: "Building BWA index for reference genome {input.genome}\n"
    shell:
        """
        singularity exec {params.bind} {params.bwa_bin} bwa index -a bwtsw -b 500000000 {input.genome}
        singularity exec {params.bind} {params.samtools_bin} samtools faidx {input.genome}
        """

rule bwa_pe :
    input:
        R1 = "{outdir}/fastp/{{sample}}_1_trim.fastq.gz".format(outdir=config["outdir"]),
        R2 = "{outdir}/fastp/{{sample}}_2_trim.fastq.gz".format(outdir=config["outdir"]),
        #fake input used to force index building before alignement if not present
        idx = config["REFPATH"] + "/" + config["GENOME"] + ".bwt"
    output:
        bam   = "{outdir}/mapped/{{sample}}_sorted.bam".format(outdir=config["outdir"]),
        stats = "{outdir}/mapped/{{sample}}.stats.txt".format(outdir=config["outdir"])
    params:
        outdir       = config["outdir"],
        idxbase      = config["REFPATH"] + "/" + config["GENOME"],
        bind         = config["BIND"],
        bwa_bin      = config["bwa_bin"],
        samtools_bin = config["samtools_bin"],
        freebayes_bin= config["freebayes_bin"], # freebayes_bin used for sambamba for markdup
        rg           = "@RG\\tID:{sample}\\tSM:{sample}"
    message: "Mapping reads {input.R1} to {params.idxbase} using BWA.\n"
    #converting to bam, sorting and removing dupplicates in a single command!
    shell:
        """
        singularity exec {params.bind} {params.bwa_bin} bwa mem \
        -t 10 \
        -K 100000000 \
        {params.idxbase} \
        {input.R1} {input.R2} \
        | singularity exec {params.bind} {params.samtools_bin} samtools sort -@2 -m 6G -o {params.outdir}/{wildcards.sample}.bam.raw -
        
        #Without rm duplicate
        singularity exec {params.bind} {params.samtools_bin} samtools index -@ 10 {params.outdir}/{wildcards.sample}.bam.raw
        mv {params.outdir}/{wildcards.sample}.bam.raw.bai {output.bam}.bai 
        mv {params.outdir}/{wildcards.sample}.bam.raw {output.bam}
        
        #with rm duplicate
        #singularity exec {params.bind} {params.samtools_bin} samtools rmdup -s {params.outdir}/{wildcards.sample}.bam.raw {output.bam}
        #singularity exec {params.bind} {params.samtools_bin} samtools index -@ 10 {output.bam}
        
        #singularity exec {params.bind} {params.freebayes_bin} sambamba markdup -t 10 {params.outdir}/{wildcards.sample}.bam.raw {output.bam}
        #singularity exec {params.bind} {params.samtools_bin} samtools index -@ 10 {output.bam}
        singularity exec {params.bind} {params.samtools_bin} samtools flagstat -@ 10 {output.bam}
        singularity exec {params.bind} {params.samtools_bin} samtools stats -@ 10 {output.bam} >{output.stats}
        rm -f {params.outdir}/{wildcards.sample}.bam.raw*
        """

rule merge_bams :
    input:
        bams=lambda wildcards: expand(expand("{outdir}/mapped/{sample}_sorted.bam", outdir=config["outdir"], sample=samples['SampleName'])),
        bamslist = "{outdir}/bams.list".format(outdir=config["outdir"])
    output:
        bam = "{outdir}/bams_merged_sorted.bam".format(outdir=config["outdir"])
    params:
        bamsp       = "{outdir}/mapped".format(outdir=config["outdir"]),
        outlist     = config["outdir"],
        samtools_bin = config["samtools_bin"],
        bind = config["BIND"]
    shell:"""
        singularity exec {params.bind} {params.samtools_bin} samtools merge -@10 -f -b {input.bamslist} {output.bam}
        singularity exec {params.bind} {params.samtools_bin} samtools index -@10 {output.bam}
        """

rule freebayes:
    input:
        #bam   = "{outdir}/mapped/{{sample}}_sorted.bam".format(outdir=config["outdir"])
        bam = "{outdir}/bams_merged_sorted.bam".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        freebayes_bin = config["freebayes_bin"]
    output:
        #var = "{outdir}/variant/{{sample}}_freebayes.vcf".format(outdir=config["outdir"])
        var = "{outdir}/variant/SNPs4all_freebayes.vcf".format(outdir=config["outdir"])
    message: "Looking for SNP in {input.bam} with freebayes \n"
    #from bam to freebayes
    shell:
        """
        singularity exec {params.bind} {params.freebayes_bin} \
        freebayes -f {params.ref} \
        --min-mapping-quality 30 \
        --min-base-quality 25 \
        --min-coverage 50 \
        --min-alternate-count 10 \
        {input.bam} > {output.var}
        """

rule multiqc_bam:
    input:
        expand("{outdir}/mapped/{sample}.stats.txt", outdir=config["outdir"], sample=samples['SampleName'])
    output:
        "{outdir}/multiqc/multiqc_report_bam.html".format(outdir=config["outdir"])
        #"{outdir}/multiqc/multiqc_report.html".format(outdir=config["outdir"])
    threads:
        1
    params:
        outdir      = config["outdir"]+"/mapped/*.stats.txt",
        modules     = config["MODULES"],
        multiqc_bin = config["multiqc_bin"],
        bind        = config["BIND"]
    shell:
        """
        #mkdir -p {params.outdir}/multiqc #snakemake create automaticly the folders
        {params.modules}
        singularity exec {params.bind} {params.multiqc_bin} multiqc --filename {output} {input}
        """

#function return fastq1 and fastq2 files and adds the fastq path
rule fastqc_pe:
    input:
        R1 = "{outdir}/fastp/{{sample}}_1_trim.fastq.gz".format(outdir=config["outdir"]),
        R2 = "{outdir}/fastp/{{sample}}_2_trim.fastq.gz".format(outdir=config["outdir"])
    output:
        # to avoid the output name generated by fastqc (regex on the name) we use a flag file
        "{outdir}/fastqc/{{sample}}.OK.done".format(outdir=config["outdir"])
    threads:
        2
    resources:
        mem_mb=4000
    params:
        fqdir      = config["fq_dir"],
        outdir     = config["outdir"],
        modules    = config["MODULES"],
        fastqc_bin = config["fastqc_bin"],
        bind       = config["BIND"]
    shell:
        """
        #mkdir -p {params.outdir}/fastqc #snakemake create automaticly the folders
        {params.modules}
        singularity exec {params.bind} {params.fastqc_bin} fastqc -o {params.outdir}/fastqc -t {threads} {input.R1} {input.R2} && touch {output}
        """

rule multiqc_fastqc:
    input:
        expand("{outdir}/fastqc/{sample}.OK.done", outdir=config["outdir"], sample=samples['SampleName'])
    output:
        report("{outdir}/multiqc/multiqc_report_fastqc.html".format(outdir=config["outdir"]), caption="report/multiqc.rst", category="Quality control")
        #"{outdir}/multiqc/multiqc_report.html".format(outdir=config["outdir"])
    threads:
        1
    params:
        outdir      = config["outdir"],
        modules     = config["MODULES"],
        multiqc_bin = config["multiqc_bin"],
        bind        = config["BIND"]
    shell:
        """
        #mkdir -p {params.outdir}/multiqc #snakemake create automaticly the folders
        {params.modules}
        singularity exec {params.bind} {params.multiqc_bin} multiqc --filename {output} {params.outdir}/fastqc
        """

