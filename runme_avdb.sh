#!/bin/bash

echo "##gff-version 3" >19genes.gff3
grep -w -f genes.list /work2/project/gafl/Data/Euphorbiaceae/Manihot_esculenta/DNA/Ref_Genome/GCF_001659605.1_Manihot_esculenta_v6_genomic.gff >>19genes.gff3
singularity exec /work2/project/gafl/tools/containers/cufflinks_v2.2.1.sif gffread 19genes.gff3 -T -o 19genes.gtf

#python3 annovar_makebd_jl.py

