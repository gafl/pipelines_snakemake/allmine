#!/bin/bash
# run snakemake bwa workflow
# use of cluster: SLURM, sample file, rules in modules: cf Snakefile.modules
# FastQC_v0.11.7 and MultiQC-v1.7 are obtained from singularity images
# using a config file in yaml
# using ressources CPU and memory
# Using cluster with SLURM cluster parametrer in a conster.json
# singularity
# Using report: include multiqc html in the report
# Samples (samples names and fastq files) in csv file


#SBATCH --job-name=allmine4sara # job name (-J)
#SBATCH --time="48:00:00" #max run time "hh:mm:ss" or "dd-hh:mm:ss" (-t)
## SBATCH --partition=workq # partition (if commented run on default partition) (-p)
#SBATCH --cpus-per-task=1 # max nb of cores (-c)
#SBATCH --ntasks=1 #nb of tasks
#SBATCH --mem=2G # max memory (-m)
#SBATCH --output=sara.%j.out # stdout (-o)

########################## on genotoul ###############################
## snakemake 5.3
#module load system/Python-3.6.3
#module load system/singularity-3.5.3
######################################################################


CONFIG=config.yaml
#RULES=Snakefile_merged2bwa
#RULES=Snakefile_merged2bwa_freebayes
RULES=Snakefile_4allmine.smk


CLUSTER_CONFIG=cluster.json
CLUSTER='sbatch --mem={cluster.mem} -t {cluster.time} -c {cluster.cores} -J {cluster.jobname} -o logs/{cluster.out}'
MAX_JOBS=4000

rm -fr .snakemake

# log directory is mandatory (see $CLUSTER) else slurm jobs failed but not the master job
mkdir -p logs

# generate the dag files
#with samples
snakemake --configfile $CONFIG -s $RULES --dag | dot -Tpdf > dag.pdf
#only rules
snakemake --configfile $CONFIG -s $RULES --rulegraph | dot -Tpdf > dag_rules.pdf

#dry run
snakemake --configfile $CONFIG -s $RULES -np -j $MAX_JOBS --cluster-config $CLUSTER_CONFIG --cluster "$CLUSTER" >snakemake_dryrun.out

#full run
snakemake --configfile $CONFIG -s $RULES -j $MAX_JOBS --cluster-config $CLUSTER_CONFIG --cluster "$CLUSTER"
#echo "snakemake --configfile $CONFIG -s $RULES -jp $MAX_JOBS --cluster-config $CLUSTER_CONFIG --cluster \"$CLUSTER\""

# If latency problem add
# --latency-wait 60

#generate a final report
#snakemake --configfile $CONFIG -s $RULES --report smk_report.html

exit 0

